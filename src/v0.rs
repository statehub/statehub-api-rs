//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::collections::HashMap;
use std::convert::Infallible;
use std::fmt;
use std::ops;
use std::str;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use serde_with::{skip_serializing_none, DeserializeFromStr, SerializeDisplay};
use statehub_location::{AwsRegion, AzureRegion, Location};
use uuid::Uuid;

pub use self::aws::PrivateLinkServiceAws;
pub use self::azure::PrivateLinkServiceAzure;
pub use self::cluster::{
    Cluster, ClusterLocationAws, ClusterLocationAzure, ClusterLocations, ClusterName, ClusterToken,
    CreateClusterDto, Provider,
};
pub use self::error::CLUSTER_IS_STATE_OWNER;
pub use self::error::CLUSTER_NOT_FOUND;
pub use self::error::NOT_AUTHORIZED;
pub use self::error::STATE_ALREADY_EXISTS;
pub use self::error::UNAUTHENTICATED;
pub use self::error::VOLUME_NOT_FOUND;
pub use self::error::{Error, Permission};
pub use self::events::{Report, Reporter};
pub use self::helm::Helm;
pub use self::login::Login;
pub use self::state::{
    Condition, CreateStateDto, CreateStateLocationAwsDto, CreateStateLocationAzureDto,
    CreateStateLocationsDto, ProvisioningStatus, State, StateLocationAws, StateLocationAzure,
    StateLocationStatus, StateLocations, StateName, StorageClass,
};
pub use self::volume::{
    CreateVolumeDto, LocationVolumeStatus, StateLocationVolumeProgress, Volume, VolumeBindingMode,
    VolumeFileSystem, VolumeLocation, VolumeName, VolumeStatus,
};

mod aws;
mod azure;
mod cluster;
mod error;
mod events;
mod helm;
mod login;
mod state;
mod volume;

pub const VERSION: &str = "/v0";
