//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

#![allow(clippy::use_self)]

use thiserror::Error;

use super::*;

mod impls;

pub const UNAUTHENTICATED: &str = "UnauthenticatedError";
pub const VOLUME_NOT_FOUND: &str = "VolumeNotFoundError";
pub const CLUSTER_IS_STATE_OWNER: &str = "ClusterIsStateOwnerError";
pub const NOT_AUTHORIZED: &str = "NotAuthorizedError";
pub const CLUSTER_NOT_FOUND: &str = "ClusterNotFoundError";
pub const STATE_ALREADY_EXISTS: &str = "StateAlreadyExistsError";

#[derive(Debug, Error, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
// #[error("{error_code}")]
pub struct Error {
    pub http_code: u16,
    pub http_status: String,
    pub error_code: String,
    pub msg: Option<String>,
    pub request_id: Option<String>,
}

#[derive(Clone, Copy, Debug, PartialEq, Hash, SerializeDisplay, DeserializeFromStr)]
pub enum Permission {
    ReadClusters,
    CreateClusters,
    DeleteClusters,
    CreateClusterToken,
    ReadClusterToken,
    DeleteClusterToken,
    ReadClusterLocations,
    UpdateClusterLocations,
    ReadStates,
    CreateStates,
    DeleteStates,
    CreateStateOwner,
    DeleteStateOwner,
    CreateStateLocations,
    ReadStateLocations,
    DeleteStateLocations,
    ReadStateLocationPrincipals,
    CreateStateLocationPrincipals,
    UpdateStateLocationPle,
    ReadVolumes,
    CreateVolumes,
    DeleteVolumes,
    UpdateVolumeActiveLocation,
    DeleteVolumeActiveLocation,
    ReadOrganization,
    UpdateOrganization,
    ReadOrganizationRole,
    ReadPersonalTokens,
    CreatePersonalTokens,
    UpdatePersonalTokens,
    DeletePersonalTokens,
    ReadInvitations,
    CreateInvitations,
    UpdateInvitations,
    DeleteInvitations,
    ReadMembers,
    CreateMembers,
    UpdateMembers,
    DeleteMembers,
    ReadProfile,
    UpdateProfile,
}
