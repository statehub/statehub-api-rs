//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use chrono::{DateTime, Utc};
use serde::Serialize;

mod impls;

#[derive(Serialize, Debug)]
pub struct Report<T> {
    pub reporter: Reporter,
    pub timestamp: DateTime<Utc>,
    pub event: T,
}

#[derive(Serialize, Debug)]
pub struct Reporter {
    pub name: String,
    pub version: String,
}

#[cfg(test)]
mod tests;
