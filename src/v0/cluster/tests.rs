//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde_json as json;

use super::*;

#[test]
fn de() {
    let clusters: Vec<Cluster> = json::from_str(CLUSTERS).unwrap();
    assert_eq!(clusters.len(), 5);
}

const CLUSTERS: &str = r#"
[
    {
        "id": "f18123b6-ac56-4596-a734-66534a287999",
        "name": "awseast212",
        "namespace": "statehub-system",
        "provider": "eks",
        "created": "2021-10-18T08:04:36.686Z",
        "modified": "2021-10-18T08:22:16.357Z",
        "locations": {
            "aws": [
                {
                    "region": "us-east-2",
                    "accountPrincipal": "arn:aws:iam::xxxxxxxx:root"
                }
            ],
            "azure": []
        },
        "helm": [
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-csi-driver",
                "version": "1.0.25",
                "parameters": {
                    "provider": "eks"
                },
                "modified": "2021-06-10T07:23:08.464Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "state-controller",
                "version": "0.0.53",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-14T08:40:34.044Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-controller",
                "version": "0.1.16",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-03T07:56:43.467Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-prereqs",
                "version": "0.0.4",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-07T10:50:37.291Z"
            }
        ]
    },
    {
        "id": "ff5b807d-4cd7-4730-96b3-72b717f686cb",
        "name": "n-3aknez",
        "provider": "aks",
        "created": "2021-10-18T06:35:57.727Z",
        "modified": "2021-10-18T06:35:57.727Z",
        "locations": {},
        "helm": [
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-csi-driver",
                "version": "1.0.25",
                "parameters": {
                    "provider": "aks"
                },
                "modified": "2021-06-10T07:23:08.464Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "state-controller",
                "version": "0.0.53",
                "parameters": {
                    "isAzure": "true"
                },
                "modified": "2021-10-14T08:40:34.251Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-controller",
                "version": "0.1.16",
                "parameters": {
                    "isAzure": "true"
                },
                "modified": "2021-10-03T07:56:43.628Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-prereqs",
                "version": "0.0.4",
                "parameters": {
                    "isAzure": "true"
                },
                "modified": "2021-10-07T10:50:37.155Z"
            }
        ]
    },
    {
        "id": "8f3c0a79-c060-46ee-ba93-ef1074645d9e",
        "name": "000",
        "provider": "aks",
        "created": "2021-10-11T14:13:53.706Z",
        "modified": "2021-10-11T14:13:53.706Z",
        "helm": [
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-csi-driver",
                "version": "1.0.25",
                "parameters": {
                    "provider": "aks"
                },
                "modified": "2021-06-10T07:23:08.464Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "state-controller",
                "version": "0.0.53",
                "parameters": {
                    "isAzure": "true"
                },
                "modified": "2021-10-14T08:40:34.251Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-controller",
                "version": "0.1.16",
                "parameters": {
                    "isAzure": "true"
                },
                "modified": "2021-10-03T07:56:43.628Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-prereqs",
                "version": "0.0.4",
                "parameters": {
                    "isAzure": "true"
                },
                "modified": "2021-10-07T10:50:37.155Z"
            }
        ]
    },
    {
        "id": "ca413226-b33d-4274-b158-4b05bc14be88",
        "name": "12011ps0",
        "provider": "eks",
        "created": "2021-10-11T14:13:52.917Z",
        "modified": "2021-10-11T14:13:52.917Z",
        "helm": [
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-csi-driver",
                "version": "1.0.25",
                "parameters": {
                    "provider": "eks"
                },
                "modified": "2021-06-10T07:23:08.464Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "state-controller",
                "version": "0.0.53",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-14T08:40:34.044Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-controller",
                "version": "0.1.16",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-03T07:56:43.467Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-prereqs",
                "version": "0.0.4",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-07T10:50:37.291Z"
            }
        ]
    },
    {
        "id": "c8ba5bef-66bd-410e-ac3c-2cc800212ac2",
        "name": "prod",
        "namespace": "statehub-system",
        "provider": "eks",
        "created": "2021-10-11T14:13:52.240Z",
        "modified": "2021-10-11T14:13:52.240Z",
        "helm": [
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-csi-driver",
                "version": "1.0.25",
                "parameters": {
                    "provider": "eks"
                },
                "modified": "2021-06-10T07:23:08.464Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "state-controller",
                "version": "0.0.53",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-14T08:40:34.044Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-controller",
                "version": "0.1.16",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-03T07:56:43.467Z"
            },
            {
                "repo": "https://helm.statehub.io",
                "chart": "statehub-prereqs",
                "version": "0.0.4",
                "parameters": {
                    "isAws": "true"
                },
                "modified": "2021-10-07T10:50:37.291Z"
            }
        ]
    }
]
"#;
