//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::cmp;
use std::iter;

use super::*;

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if f.alternate() {
            f.debug_struct("State")
                .field("name", &self.name.0)
                .field("aws", &self.locations.aws)
                .field("azure", &self.locations.azure)
                .finish()
        } else {
            self.name.fmt(f)
        }
    }
}

impl From<String> for StateName {
    fn from(name: String) -> Self {
        Self(name)
    }
}

impl From<&str> for StateName {
    fn from(text: &str) -> Self {
        text.to_string().into()
    }
}

impl fmt::Display for StateName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl str::FromStr for StateName {
    type Err = Infallible;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        Ok(text.into())
    }
}

impl AsRef<str> for StateName {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl ops::Deref for StateName {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.0.deref()
    }
}

impl PartialEq<&str> for StateName {
    fn eq(&self, other: &&str) -> bool {
        self.0.eq(other)
    }
}

impl State {
    pub fn is_available_in(&self, location: &Location) -> bool {
        self.locations.contains(location)
    }

    pub fn is_ready(&self) -> bool {
        self.condition == Condition::Green
    }

    pub fn all_locations(&self) -> Vec<Location> {
        let aws = self.locations.aws.iter().map(|aws| aws.region.into());
        let azure = self.locations.azure.iter().map(|azure| azure.region.into());
        aws.chain(azure).collect()
    }

    pub fn collect_volumes(&self) -> HashMap<String, HashMap<Location, &VolumeLocation>> {
        let aws = self.locations.aws.iter().flat_map(|location| {
            location
                .volumes
                .iter()
                .map(move |volume| (volume, location.region.into()))
        });
        let azure = self.locations.azure.iter().flat_map(|location| {
            location
                .volumes
                .iter()
                .map(move |volume| (volume, location.region.into()))
        });

        let mut volumes = HashMap::<_, HashMap<_, _>>::new();
        for (volume, location) in aws.chain(azure) {
            volumes
                .entry(volume.name.clone())
                .or_default()
                .insert(location, volume);
        }
        volumes
    }

    pub fn count_volumes(&self) -> usize {
        let aws = self
            .locations
            .aws
            .iter()
            .map(|aws| aws.volumes.len())
            .max()
            .unwrap_or_default();
        let azure = self
            .locations
            .azure
            .iter()
            .map(|azure| azure.volumes.len())
            .max()
            .unwrap_or_default();
        cmp::max(aws, azure)
    }
}

impl StateLocationStatus {
    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Ok => "ok",
            Self::Provisioning => "provisioning",
            Self::Recovering => "recovering",
            Self::Deleting => "deleting",
            Self::Error => "error",
        }
    }

    pub fn is_deleting(&self) -> bool {
        *self == Self::Deleting
    }

    pub fn is_final(&self) -> bool {
        match self {
            Self::Ok => true,
            Self::Provisioning => false,
            Self::Recovering => false,
            Self::Deleting => false,
            Self::Error => true,
        }
    }
}

impl Default for VolumeBindingMode {
    fn default() -> Self {
        Self::WaitForFirstConsumer
    }
}

impl From<AwsRegion> for CreateStateLocationAwsDto {
    fn from(region: AwsRegion) -> Self {
        Self { region }
    }
}

impl From<AzureRegion> for CreateStateLocationAzureDto {
    fn from(region: AzureRegion) -> Self {
        Self { region }
    }
}

impl iter::FromIterator<Location> for CreateStateLocationsDto {
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item = Location>,
    {
        iter.into_iter().fold(Self::default(), |mut dto, location| {
            match location {
                Location::Aws(region) => dto.aws.push(region.into()),
                Location::Azure(region) => dto.azure.push(region.into()),
                // Location::Gcp(region) => dto.gcp.push(region),
            }
            dto
        })
    }
}

impl StateLocations {
    pub fn contains(&self, location: &Location) -> bool {
        match location {
            Location::Aws(region) => self.aws.iter().any(|aws| aws.region == *region),
            Location::Azure(region) => self.azure.iter().any(|azure| azure.region == *region),
        }
    }
}

impl Default for Condition {
    fn default() -> Self {
        Self::Green
    }
}

impl Default for ProvisioningStatus {
    fn default() -> Self {
        Self::Ready
    }
}

impl StorageClass {
    pub fn new_if_not_default(
        state: &str,
        storage_class: Option<String>,
        fs_type: Option<VolumeFileSystem>,
    ) -> Option<Self> {
        if storage_class.is_none() && fs_type.is_none() {
            return None;
        }

        let name = storage_class.unwrap_or_else(|| state.to_string());
        let fs_type = fs_type.unwrap_or_default().to_string();

        let storage_class = Self {
            name,
            fs_type,
            ..Self::default()
        };

        Some(storage_class)
    }
}
