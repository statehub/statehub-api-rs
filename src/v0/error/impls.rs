//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::fmt;

use serde_json as json;

use super::*;

impl Error {
    pub fn is_unauthenticated(&self) -> bool {
        self.error_code == UNAUTHENTICATED
    }

    pub fn is_unauthorized(&self) -> bool {
        self.error_code == NOT_AUTHORIZED
    }

    pub fn is_volume_not_found(&self) -> bool {
        self.error_code == VOLUME_NOT_FOUND
    }

    pub fn is_cluster_not_found(&self) -> bool {
        self.error_code == CLUSTER_NOT_FOUND
    }

    pub fn cluster_is_state_owner(&self) -> bool {
        self.error_code == CLUSTER_IS_STATE_OWNER
    }

    pub fn state_already_exists(&self) -> bool {
        self.error_code == STATE_ALREADY_EXISTS
    }

    pub fn unknown_error(status: http::StatusCode, bytes: &[u8]) -> Self {
        let http_code = status.as_u16();
        let http_status = status
            .canonical_reason()
            .unwrap_or_else(|| status.as_str())
            .to_string();

        let (error_code, msg, request_id) = match json::from_slice::<json::Value>(bytes) {
            Ok(ref value) => (
                get(value, "errorCode"),
                get(value, "msg"),
                get(value, "requestId"),
            ),
            Err(e) => {
                let msg = format!("{e}\nCheck your config file (~/.statehub/config.toml by default) for valid api server url");
                (None, Some(msg), None)
            }
        };
        let error_code = error_code.unwrap_or_else(|| http_status.clone());

        Self {
            http_code,
            http_status,
            error_code,
            msg,
            request_id,
        }
    }
}

fn get(value: &json::Value, index: &str) -> Option<String> {
    value.get(index)?.as_str().map(String::from)
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let request_id = self
            .request_id
            .as_ref()
            .map(|request_id| format!("\nRequest id: {request_id}"))
            .unwrap_or_default();
        let msg = self
            .msg
            .as_ref()
            .map(|msg| format!(": {msg}"))
            .unwrap_or_default();
        let error_code = &self.error_code;
        format!("{error_code}{msg}{request_id}").fmt(f)
    }
}

impl Permission {
    pub fn as_str(&self) -> &'static str {
        match self {
            Self::ReadClusters => "read:clusters",
            Self::CreateClusters => "create:clusters",
            Self::DeleteClusters => "delete:clusters",
            Self::CreateClusterToken => "create:cluster_token",
            Self::ReadClusterToken => "read:cluster_token",
            Self::DeleteClusterToken => "delete:cluster_token",
            Self::ReadClusterLocations => "read:cluster_locations",
            Self::UpdateClusterLocations => "update:cluster_locations",
            Self::ReadStates => "read:states",
            Self::CreateStates => "create:states",
            Self::DeleteStates => "delete:states",
            Self::CreateStateOwner => "create:state_owner",
            Self::DeleteStateOwner => "delete:state_owner",
            Self::CreateStateLocations => "create:state_locations",
            Self::ReadStateLocations => "read:state_locations",
            Self::DeleteStateLocations => "delete:state_locations",
            Self::ReadStateLocationPrincipals => "read:state_location_principals",
            Self::CreateStateLocationPrincipals => "create:state_location_principals",
            Self::UpdateStateLocationPle => "update:state_location_ple",
            Self::ReadVolumes => "read:volumes",
            Self::CreateVolumes => "create:volumes",
            Self::DeleteVolumes => "delete:volumes",
            Self::UpdateVolumeActiveLocation => "update:volume_active_location",
            Self::DeleteVolumeActiveLocation => "delete:volume_active_location",
            Self::ReadOrganization => "read:organization",
            Self::UpdateOrganization => "update:organization",
            Self::ReadOrganizationRole => "read:organization_roles",
            Self::ReadPersonalTokens => "read:personal_tokens",
            Self::CreatePersonalTokens => "create:personal_tokens",
            Self::UpdatePersonalTokens => "update:personal_tokens",
            Self::DeletePersonalTokens => "delete:personal_tokens",
            Self::ReadInvitations => "read:invitations",
            Self::CreateInvitations => "create:invitations",
            Self::UpdateInvitations => "update:invitations",
            Self::DeleteInvitations => "delete:invitations",
            Self::ReadMembers => "read:members",
            Self::CreateMembers => "create:members",
            Self::UpdateMembers => "update:members",
            Self::DeleteMembers => "delete:members",
            Self::ReadProfile => "read:profile",
            Self::UpdateProfile => "update:profile",
        }
    }
}

impl fmt::Display for Permission {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_str().fmt(f)
    }
}

#[derive(Debug, Error)]
#[error(r#"Invalid permission "{permission}"#)]
pub struct InvalidPermission {
    pub permission: String,
}

impl InvalidPermission {
    pub(crate) fn new(permission: &str) -> Self {
        let permission = permission.to_string();
        Self { permission }
    }
}

impl str::FromStr for Permission {
    type Err = InvalidPermission;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        match text {
            "read:clusters" => Ok(Self::ReadClusters),
            "create:clusters" => Ok(Self::CreateClusters),
            "delete:clusters" => Ok(Self::DeleteClusters),
            "create:cluster_token" => Ok(Self::CreateClusterToken),
            "read:cluster_token" => Ok(Self::ReadClusterToken),
            "delete:cluster_token" => Ok(Self::DeleteClusterToken),
            "read:cluster_locations" => Ok(Self::ReadClusterLocations),
            "update:cluster_locations" => Ok(Self::UpdateClusterLocations),
            "read:states" => Ok(Self::ReadStates),
            "create:states" => Ok(Self::CreateStates),
            "delete:states" => Ok(Self::DeleteStates),
            "create:state_owner" => Ok(Self::CreateStateOwner),
            "delete:state_owner" => Ok(Self::DeleteStateOwner),
            "create:state_locations" => Ok(Self::CreateStateLocations),
            "read:state_locations" => Ok(Self::ReadStateLocations),
            "delete:state_locations" => Ok(Self::DeleteStateLocations),
            "read:state_location_principals" => Ok(Self::ReadStateLocationPrincipals),
            "create:state_location_principals" => Ok(Self::CreateStateLocationPrincipals),
            "update:state_location_ple" => Ok(Self::UpdateStateLocationPle),
            "read:volumes" => Ok(Self::ReadVolumes),
            "create:volumes" => Ok(Self::CreateVolumes),
            "delete:volumes" => Ok(Self::DeleteVolumes),
            "update:volume_active_location" => Ok(Self::UpdateVolumeActiveLocation),
            "delete:volume_active_location" => Ok(Self::DeleteVolumeActiveLocation),
            "read:organization" => Ok(Self::ReadOrganization),
            "update:organization" => Ok(Self::UpdateOrganization),
            "read:organization_roles" => Ok(Self::ReadOrganizationRole),
            "read:personal_tokens" => Ok(Self::ReadPersonalTokens),
            "create:personal_tokens" => Ok(Self::CreatePersonalTokens),
            "update:personal_tokens" => Ok(Self::UpdatePersonalTokens),
            "delete:personal_tokens" => Ok(Self::DeletePersonalTokens),
            "read:invitations" => Ok(Self::ReadInvitations),
            "create:invitations" => Ok(Self::CreateInvitations),
            "update:invitations" => Ok(Self::UpdateInvitations),
            "delete:invitations" => Ok(Self::DeleteInvitations),
            "read:members" => Ok(Self::ReadMembers),
            "create:members" => Ok(Self::CreateMembers),
            "update:members" => Ok(Self::UpdateMembers),
            "delete:members" => Ok(Self::DeleteMembers),
            "read:profile" => Ok(Self::ReadProfile),
            "update:profile" => Ok(Self::UpdateProfile),
            other => Err(InvalidPermission::new(other)),
        }
    }
}

#[cfg(test)]
mod tests {
    use serde_json as json;

    use super::*;

    #[test]
    fn invalid_token() {
        let text = r#"{"httpCode":401,"httpStatus":"Unauthorized","errorCode":"UnauthenticatedError","msg":"","requestId":"e4900455157d450e9a5285153b99225c"}"#;
        let err: Error = json::from_str(text).unwrap();
        assert!(err.is_unauthenticated());
    }

    #[test]
    fn cluster_not_authorized1() {
        let text = r#"{"httpCode":403,"httpStatus":"Forbidden","errorCode":"NotAuthorizedError","msg":"Not authorized to perform undefined","requestId":"e4900455157d450e9a5285153b99225c"}"#;
        let err: Error = json::from_str(text).unwrap();
        assert!(err.is_unauthorized());
    }

    #[test]
    fn cluster_not_found() {
        let text = r#"{"httpCode":404,"httpStatus":"Not Found","errorCode":"ClusterNotFoundError","msg":"Cluster undefined not found","requestId":"e4900455157d450e9a5285153b99225c"}"#;
        let err: Error = json::from_str(text).unwrap();
        assert!(err.is_cluster_not_found());
    }

    #[test]
    fn cluster_is_state_owner() {
        let text = r#"{"httpCode":409,"httpStatus":"Conflict","errorCode":"ClusterIsStateOwnerError","msg":"Cluster undefined is currently the owner of state undefined","requestId":"e4900455157d450e9a5285153b99225c"}"#;
        let err: Error = json::from_str(text).unwrap();
        assert!(err.cluster_is_state_owner());
    }

    #[test]
    fn satte_already_exists() {
        let text = r#"{"httpCode":409,"httpStatus":"Conflict","errorCode":"StateAlreadyExistsError","msg":"State undefined already exists","requestId":"e4900455157d450e9a5285153b99225c"}"#;

        let err: Error = json::from_str(text).unwrap();
        assert!(err.state_already_exists());
    }

    #[test]
    fn volume_not_found() {
        let text = r#"{"httpCode": 404,"httpStatus": "Not Found","errorCode": "VolumeNotFoundError","msg": "Volume undefined not found","requestId": "e4900455157d450e9a5285153b99225c"}"#;
        let err: Error = json::from_str(text).unwrap();
        assert!(err.is_volume_not_found());
    }
}
