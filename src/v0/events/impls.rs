//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl<T> Report<T> {
    pub fn new(reporter: impl ToString, version: impl ToString, event: T) -> Self {
        let name = reporter.to_string();
        let version = version.to_string();
        let reporter = Reporter { name, version };
        let timestamp = Utc::now();
        Self {
            reporter,
            timestamp,
            event,
        }
    }
}
