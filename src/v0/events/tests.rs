//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

// use std::time;

use serde_json as json;
use serde_with::{serde_as, DisplayFromStr};

use super::*;

#[serde_as]
#[derive(Serialize)]
struct TestEvent {
    #[serde_as(as = "DisplayFromStr")]
    duration: chrono::Duration,
    message: String,
}

impl TestEvent {
    fn new(message: &str) -> Self {
        let duration = chrono::Duration::seconds(3);
        let message = message.to_string();
        Self { duration, message }
    }
}

#[test]
fn json_report() {
    let event = TestEvent::new("foo");
    let report = Report::new("bozo", "0.0.4", event);
    println!("{}", json::to_string_pretty(&report).unwrap());
    let value = json::to_value(&report).unwrap();
    assert_eq!(value["event"]["duration"], "PT3S");
}
